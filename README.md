# F# new project creation

## Table of contents

1. [Solution](#solution)
   1. [Creation](#creation)
2. [Classlib](#classlib)
   1. [Creation](#creation)
   2. [Add to Solution](#add-to-solution)
   3. [Add new package from Nuget](#add-new-package-from-nuget)
3. [Console Application](#console-application)
   1. [Creation](#creation)
   2. [Create Library refence](#create-library-refence)
   3. [Add to Solution](#add-to-solution)
4. [Run Project](#run-project)
5. [Project structure example](#project-structure-example)
6. [Links](#links)

## Solution

### Creation

```
dotnet new sln -o HousePriceAnalizyer
cd HousePriceAnalizyer
```

## Classlib

### Creation

```
dotnet new classlib -lang "F#" -o src/Library
```

### Add to Solution

```
dotnet sln add src/Library/Library.fsproj
```

### Add new package from Nuget

```
dotnet add src/Library/Library.fsproj package FSharp.Data --version 4.2.2
```

## Console Application

### Creation

```
dotnet new console -lang "F#" -o src/App
```

### Create Library refence

```
dotnet add src/App/App.fsproj reference src/Library/Library.fsproj
```

### Add to Solution

```
dotnet sln add src/App/App.fsproj
```

## Run Project

```
cd src/App
dotnet run Hello World
```

## Project structure example

```
├── HousePriceAnalizyer.sln
├── README.md
└── src
    ├── App
    │   ├── App.fsproj
    │   └── Program.fs
    └── Library
        ├── Library.fs
        └── Library.fsproj
```

## Links

- [Microsoft Docs](https://docs.microsoft.com/pt-br/dotnet/fsharp/get-started/get-started-command-line)
