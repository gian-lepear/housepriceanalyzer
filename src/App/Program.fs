open System.Text.Json
open Library

[<EntryPoint>]
let main _ =

    let jsonOptions = JsonSerializerOptions()
    jsonOptions.Encoder <- System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All)

    let json =
        JsonSerializer.Serialize(Apartament.PageResults, jsonOptions)

    System.IO.File.WriteAllText("result.json", json)
    0 // return an integer exit code
