﻿namespace Library

open FSharp.Data

type Apartament =
    { Price: decimal
      City: string
      District: string
      Area: int }

module Apartament =
    [<Literal>]
    let Url =
        "https://www.zapimoveis.com.br/venda/imoveis/sp+sao-paulo/"

    let HtmlDoc = HtmlDocument.Load(Url)

    let FetchElemByCss (cssSelector: string) (elem: HtmlNode) : string =
        let elements = elem.CssSelect cssSelector

        match elements with
        | [] -> ""
        | _ -> elements.Head.DirectInnerText().Trim()

    let FetchPrice (elem: HtmlNode) : decimal =
        let formatStringDecimal (value: string) : decimal =
            value
                .Replace("R$ ", "")
                .Replace(".", "")
                .Replace(",", ".")
                .Trim()
            |> decimal

        elem
        |> FetchElemByCss "div.simple-card__prices > p.simple-card__price > strong"
        |> formatStringDecimal

    let FetchArea (elem: HtmlNode) =
        elem
        |> FetchElemByCss "li.feature__item > span[itemprop='floorSize']"
        |> fun value -> value.Replace("m²", "").Trim().Split [| '-' |]
        |> function
            | [| a: string |] -> int a
            | _ -> 0

    let PageResults =
        HtmlDoc.CssSelect("div.card-container")
        |> List.map
            (fun elem ->
                { Price = FetchPrice elem
                  City = "São Paulo"
                  District = "São Paulo"
                  Area = FetchArea elem })
